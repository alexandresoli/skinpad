//
//  DetalhesViewController.h
//  SkinPad
//
//  Created by Alexandre Oliveira on 9/4/12.
//  Copyright (c) 2012 Alexandre Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetalhesViewController : UIViewController <UIPopoverControllerDelegate>


@property (strong, nonatomic) UIPopoverController *popoverController;

@property (weak, nonatomic) IBOutlet UIButton *changeDeviceButton;
- (IBAction)selecionarDispositivo:(id)sender;
- (IBAction)voltar:(id)sender;



@end
