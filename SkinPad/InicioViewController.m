//
//  ViewController.m
//  SkinPad
//
//  Created by Alexandre Oliveira on 9/4/12.
//  Copyright (c) 2012 Alexandre Oliveira. All rights reserved.
//

#import "InicioViewController.h"
#import "DetalhesViewController.h"

@interface InicioViewController ()

@end

@implementation InicioViewController

@synthesize navController;

- (void)viewDidLoad
{
    [super viewDidLoad];
        self.view.backgroundColor = [UIColor underPageBackgroundColor];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (IBAction)iniciar:(id)sender {
    
    DetalhesViewController *controller = [[DetalhesViewController alloc] initWithNibName:@"DetalhesViewController_iPad" bundle:nil];
    navController = [[UINavigationController alloc] initWithRootViewController:controller];
    [navController setNavigationBarHidden:YES animated:YES];
    [self presentModalViewController:navController animated:YES];
}
@end
