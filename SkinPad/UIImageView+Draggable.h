//
//  UIImageView+Draggable.h
//  SkinPad
//
//  Created by Alexandre Oliveira on 10/8/12.
//  Copyright (c) 2012 Alexandre Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Draggable)

@end
