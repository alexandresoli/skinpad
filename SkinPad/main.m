//
//  main.m
//  SkinPad
//
//  Created by Alexandre Oliveira on 9/4/12.
//  Copyright (c) 2012 Alexandre Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
