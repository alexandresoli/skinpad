//
//  AppDelegate.h
//  SkinPad
//
//  Created by Alexandre Oliveira on 9/4/12.
//  Copyright (c) 2012 Alexandre Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "InicioViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) InicioViewController *inicioViewController;

@end
