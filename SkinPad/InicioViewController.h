//
//  ViewController.h
//  SkinPad
//
//  Created by Alexandre Oliveira on 9/4/12.
//  Copyright (c) 2012 Alexandre Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InicioViewController : UIViewController
- (IBAction)iniciar:(id)sender;
@property (strong, nonatomic) UINavigationController *navController;
@end
