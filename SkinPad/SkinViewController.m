//
//  SkinViewController.m
//  SkinPad
//
//  Created by Alexandre Oliveira on 10/8/12.
//  Copyright (c) 2012 Alexandre Oliveira. All rights reserved.
//

#import "SkinViewController.h"
#import "UIImageView+Draggable.h"


@interface SkinViewController ()

@end

@implementation SkinViewController

@synthesize caseImageView;
@synthesize popoverController;
@synthesize changeImageButton;
@synthesize photoImagevView;
@synthesize topDarkImageView,bottomDarkImageView,leftDarkImageView,rightDarkImageView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIImage *image = [UIImage imageNamed:@"iphone4_case"];
    caseImageView.image = image;
    caseImageView.bounds = CGRectMake(0, 0, image.size.width, image.size.height);

    [self addGestures];
    
 //   [self configureView:[UIDevice currentDevice].orientation];

}

//    [caseImageView sizeToFit];
//  [scrollView setContentSize:caseImageView.image.size];
//    caseImageView.frame = CGRectMake(0, 0, caseImageView.image.size.width, caseImageView.image.size.height);
//    caseImageView.frame = CGRectMake(caseImageView.frame.origin.x, caseImageView.frame.origin.y, image.size.width, image.size.height);

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)configureView:(UIInterfaceOrientation)toInterfaceOrientation
{
    
    if( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
    {
     
        topDarkImageView.frame = CGRectMake(0, 44, 1024, caseImageView.frame.origin.y - topDarkImageView.frame.origin.y);
        bottomDarkImageView.frame = CGRectMake(0, caseImageView.frame.origin.y + caseImageView.frame.size.height, 1024, bottomDarkImageView.frame.origin.y + self.view.frame.size.height);
        leftDarkImageView.frame = CGRectMake(0, topDarkImageView.frame.origin.y + topDarkImageView.frame.size.height, caseImageView.frame.origin.x, bottomDarkImageView.frame.origin.y - (topDarkImageView.frame.origin.y + topDarkImageView.frame.size.height));
        rightDarkImageView.frame = CGRectMake(caseImageView.frame.origin.x + caseImageView.frame.size.width, topDarkImageView.frame.origin.y + topDarkImageView.frame.size.height, caseImageView.frame.origin.x, bottomDarkImageView.frame.origin.y - (topDarkImageView.frame.origin.y + topDarkImageView.frame.size.height));

    }
    else
    {

        
        topDarkImageView.frame = CGRectMake(0, 44, 768, 158);
        bottomDarkImageView.frame = CGRectMake(0, 801, 768, 203);
        leftDarkImageView.frame = CGRectMake(0, topDarkImageView.frame.origin.y + topDarkImageView.frame.size.height, caseImageView.frame.origin.x - leftDarkImageView.frame.origin.x, 600);
        rightDarkImageView.frame = CGRectMake(caseImageView.frame.origin.x + caseImageView.frame.size.width, topDarkImageView.frame.origin.y + topDarkImageView.frame.size.height, caseImageView.frame.origin.x, bottomDarkImageView.frame.origin.y - bottomDarkImageView.frame.size.height);
        
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                         duration:(NSTimeInterval)duration
{

    
    [self configureView:toInterfaceOrientation];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}


- (void)viewDidUnload {
    [self setChangeImageButton:nil];
    [self setPhotoImagevView:nil];
    [self setCaseImageView:nil];
    [self setLeftDarkImageView:nil];
    [self setTopDarkImageView:nil];
    [self setBottomDarkImageView:nil];
    [self setLeftDarkImageView:nil];
    [self setRightDarkImageView:nil];
    [super viewDidUnload];
}


- (void)viewDidAppear:(BOOL)animated
{
//    [self showCameraRoll:changeImageButton];
}

#pragma mark IBAction Methods
- (IBAction)finish:(UIButton *)sender
{

}

- (IBAction)close:(UIButton *)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)showCameraRoll:(UIButton *)sender
{
    // create an image picker controller
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = (id)self;
    imagePickerController.allowsEditing = NO;
    
    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
    self.popoverController.delegate = self;
    [self.popoverController presentPopoverFromRect:changeImageButton.bounds inView:changeImageButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


#pragma mark UIImagePickerControllerDelegate Methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if([mediaType isEqualToString:@"public.image"]) {
        
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        CGSize destinationSize = caseImageView.frame.size;
        UIGraphicsBeginImageContext(destinationSize);
        [image drawInRect:CGRectMake(0, 0, destinationSize.width, destinationSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        photoImagevView.image = newImage;
        
        [self.popoverController dismissPopoverAnimated:YES];
    }
    
}
       

#pragma mark UIGestureRecognizerDelegate Methods

- (void)addGestures
{
    UIRotationGestureRecognizer *rotationGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotatePiece:)];
    [rotationGesture setDelegate:self];
    [photoImagevView addGestureRecognizer:rotationGesture];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scalePiece:)];
    [pinchGesture setDelegate:self];
    [photoImagevView addGestureRecognizer:pinchGesture];;
}


- (void)rotatePiece:(UIRotationGestureRecognizer *)gestureRecognizer {
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        [gestureRecognizer view].transform = CGAffineTransformRotate([[gestureRecognizer view] transform], [gestureRecognizer rotation]);
        [gestureRecognizer setRotation:0];
    }
}

- (void)scalePiece:(UIPinchGestureRecognizer *)gestureRecognizer {
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        [gestureRecognizer view].transform = CGAffineTransformScale([[gestureRecognizer view] transform], [gestureRecognizer scale], [gestureRecognizer scale]);
        [gestureRecognizer setScale:1];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    // if the gesture recognizers are on different views, don't allow simultaneous recognition
    if (gestureRecognizer.view != otherGestureRecognizer.view)
        return NO;
    
    // if either of the gesture recognizers is the long press, don't allow simultaneous recognition
    if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] || [otherGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
        return NO;
    
    return YES;
}

@end
