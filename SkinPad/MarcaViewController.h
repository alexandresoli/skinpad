//
//  MarcaViewController.h
//  SkinPad
//
//  Created by Alexandre Oliveira on 9/4/12.
//  Copyright (c) 2012 Alexandre Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarcaViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *listaMarcas;

@end
