//
//  DetalhesViewController.m
//  SkinPad
//
//  Created by Alexandre Oliveira on 9/4/12.
//  Copyright (c) 2012 Alexandre Oliveira. All rights reserved.
//

#import "DetalhesViewController.h"
#import "CategoriaViewController.h"

@interface DetalhesViewController ()

@end

@implementation DetalhesViewController

@synthesize popoverController;
@synthesize changeDeviceButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [self setChangeDeviceButton:nil];
    [self setPopoverController:nil];
    [super viewDidUnload];

}

- (void)viewDidAppear:(BOOL)animated
{
    [self selecionarDispositivo:changeDeviceButton];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)selecionarDispositivo:(id)sender {
    
    if (self.popoverController == nil) {
        
        CategoriaViewController *controller = [[CategoriaViewController alloc] initWithNibName:@"CategoriaViewController_iPad" bundle:nil];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        
        popoverController = [[UIPopoverController alloc] initWithContentViewController:navigationController];
        [popoverController setDelegate:self];
    }

    UIButton *button = (UIButton *)sender;
    [self.popoverController presentPopoverFromRect:button.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)voltar:(id)sender {
    
    [self dismissModalViewControllerAnimated:YES];
}
@end
