//
//  SkinViewController.h
//  SkinPad
//
//  Created by Alexandre Oliveira on 10/8/12.
//  Copyright (c) 2012 Alexandre Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SkinViewController : UIViewController <UIImagePickerControllerDelegate,UIPopoverControllerDelegate,UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIPopoverController *popoverController;
@property (weak, nonatomic) IBOutlet UIImageView *caseImageView;
@property (weak, nonatomic) IBOutlet UIButton *changeImageButton;
@property (weak, nonatomic) IBOutlet UIImageView *photoImagevView;
@property (weak, nonatomic) IBOutlet UIImageView *topDarkImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomDarkImageView;
@property (weak, nonatomic) IBOutlet UIImageView *leftDarkImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rightDarkImageView;


- (IBAction)finish:(UIButton *)sender;
- (IBAction)close:(UIButton *)sender;
- (IBAction)showCameraRoll:(UIButton *)sender;
@end
